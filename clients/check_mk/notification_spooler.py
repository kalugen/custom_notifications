#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import time
import glob
import signal
import logging
import logging.handlers
import traceback
import zmq
from zmq import ZMQError

class NotificationSpooler(object):

    def __init__(self):
        self.kill_now = False
        self.install_signal_handlers()

        self.SITE = os.getenv('OMD_SITE')
        self.BATCH_SIZE = os.getenv('NOTIFICATIONS_BATCH_SIZE', 500)
        self.ZMQSERVER = os.getenv('NOTIFICATIONS_SERVER', "tcp://mxtwbsmco01.mxs.svi.it:5570")
        self.LOGFILE = os.getenv('NOTIFICATIONS_LOG', "/omd/sites/%s/var/log/notification.spooler.log" % self.SITE)
        self.SPOOLDIR = os.getenv('NOTIFICATIONS_SPOOLDIR', "/omd/sites/%s/var/check_mk/bsm_integration/spool" % self.SITE)
        self.SENTDIR = os.getenv('NOTIFICATIONS_SENTDIR', "/omd/sites/%s/var/check_mk/bsm_integration/sent"  % self.SITE)
        self.ERRORDIR = os.getenv('NOTIFICATIONS_ERRORDIR', "/omd/sites/%s/var/check_mk/bsm_integration/error" % self.SITE)
        self.FREQUENCY  = os.getenv('NOTIFICATIONS_FREQUENCY', 0.5)
        self.TOKEN = os.getenv('NOTIFICATIONS_TOKEN', 'CHANGE_ME')

        HANDLER = logging.handlers.TimedRotatingFileHandler(self.LOGFILE, when='midnight')
        FORMATTER = logging.Formatter("%(asctime)s %(levelname)s: %(message)s")
        HANDLER.setFormatter(FORMATTER)

        LEVEL = os.getenv('NOTIFICATIONS_LOG_LEVEL', 'INFO')

        self.identity = 'notification_spooler-%s' % (self.SITE.lower())

        self.logger = logging.getLogger(self.identity)
        self.logger.setLevel(getattr(logging, LEVEL))
        self.logger.addHandler(HANDLER)

        self.poller = zmq.Poller()

        self.connect()

    def connect(self):
        # Connessione base
        context = zmq.Context()
        self.client = context.socket(zmq.DEALER)
        self.client.identity = self.identity.encode('utf-8')
        self.client.connect(self.ZMQSERVER)

        # Poller to avoid waiting forever in case the server
        # goes down before it could send us a confirmation
        self.poller.register(self.client, zmq.POLLIN)

    def disconnect(self):
        self.poller.unregister(self.client)
        self.client.setsockopt(zmq.LINGER, 0)
        self.client.close()

    def send_message(self, msgid, msg):
        '''spedisce un messaggio e attende un messaggio di conferma dal server'''
        # Send without blocking: in case the server cannot receive the message
        # this will still asynchrnously raise a ZMQError with a errno of zmq.EAGAIN
        self.client.send_multipart([self.TOKEN, msgid, msg])
        self.logger.debug('[%s] Message sent', msgid)

        # Waits for up to 5 seconds for the server to senda a confirmation message
        # or raises an exception
        sockets = dict(self.poller.poll(5000))

        if self.client in sockets and sockets[self.client] == zmq.POLLIN:
            self.logger.debug('[%s] Reading response from the server', msgid)
            (replyid, reply) = self.client.recv_multipart()

        else:
            raise ZMQError(zmq.EAGAIN)

        if replyid == msgid:
            if reply == 'OK':
                self.logger.debug("[%s] Messaggio correttamente ricevuto dal server", msgid)
                return True
            elif 'KO' in reply:
                error = reply.split(':')[1]
                self.logger.error('[%s] %s', msgid, error)
                return False
        else:
            self.logger.warning("[%s] Risposta del server malformata: %s - %s", msgid, replyid, reply)
            return False

    def install_signal_handlers(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)
        signal.signal(signal.SIGHUP, self.exit_gracefully)

    def exit_gracefully(self, signo, frame):
        self.logger.debug('[MAIN] Exit requested Signal: %s Frame: %s', signo, traceback.format_stack(frame))
        self.kill_now = True

    def run(self):
        '''this is the main application loop. the try/except block is INSIDE the loop
             since we want to interrupt execution during the message processing flow and
             still try to resend it until manually interrupted or until the server becomes
             available again and sends confirmation'''

        self.logger.info('[MAIN] CheckMK Notifications Spooler starting')

        while True:
            # Notifications directory monitor frequency
            time.sleep(int(self.FREQUENCY))

            # Check if there is something to process: we look only for files with
            # the ".ready" extension: we considered other files incomplete or temporary
            notifications = glob.glob(self.SPOOLDIR + '/*.ready')

            if len(notifications) > 0:
                self.logger.debug('[MAIN] Trovate %d notifiche', len(notifications))

                try:
                    notifications.sort()
                    # Works only BATCH_SIZE messages every cycle 
                    for notification_file in notifications[0:self.BATCH_SIZE]:
                        # we get msgid from part of the filename, since it is already unique it is a good ID.
                        msgid = os.path.basename(notification_file).split('.')[0]
                        self.logger.debug('[%s] Processing notification', msgid)

                        # Compose the message content from the notification XML file, prepending the ID.
                        with open(notification_file, 'r') as notification:
                            self.logger.debug('[%s] Reading notification', msgid)
                            msg = notification.read()
                            self.logger.debug('[%s] Message content:\n%s', msgid, msg)

                        # Send it to the server and move the notification in the appropriate directory
                        # NOTE: send_message can be interrupted by an exception from ZMQ, eg. because
                        #       the server has dropped the connection. In that case the notification
                        #       would NOT be moved and this client will try to send it again and again
                        #       until the server finally responds some way or another.
                        if self.send_message(msgid, msg):
                            os.rename(notification_file, '%s/%s' % (self.SENTDIR, os.path.basename(notification_file)))
                        else:
                            os.rename(notification_file, '%s/%s' % (self.ERRORDIR, os.path.basename(notification_file)))

                except ZMQError as z:
                    self.logger.error('[MAIN] Ricevuta exception: %s', str(z))
                    if z.errno == zmq.EAGAIN:
                        self.logger.error('[MAIN] Problemi nella connessione al server: %s', str(z))
                        self.disconnect()
                        time.sleep(1)
                        self.connect()
                    else:
                        raise z

            if self.kill_now:
                self.logger.info('[MAIN] Exiting now')
                self.disconnect()
                break

if __name__ == '__main__':
    spooler = NotificationSpooler()
    spooler.run()
