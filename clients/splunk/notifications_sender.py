#!/usr/bin/python

# System imports
import os
import sys
import csv
import gzip
import time
import glob
import socket
import signal
import codecs
import hashlib
import cStringIO
import logging
import logging.handlers
import logging.config

# Custom imports
import zmq
from zmq import ZMQError
from lxml import etree

# Set default encoding
reload(sys)
sys.setdefaultencoding('utf8')

class UTF8Recoder(object): # pylint: disable=too-few-public-methods
    '''
    Iterator that reads an encoded stream and reencodes the input to UTF-8
    '''
    def __init__(self, f, encoding):
        self.reader = codecs.getreader(encoding)(f)

    def __iter__(self):
        return self

    def next(self):
        return self.reader.next().encode("utf-8")

class UnicodeDictReader(object): # pylint: disable=too-few-public-methods
    '''
    A CSV reader which will iterate over lines in the CSV file "f", which is encoded
    in the given encoding. The default DictReader does not support encodings.
    '''
    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        f = UTF8Recoder(f, encoding)
        self.reader = csv.DictReader(f, dialect=dialect, **kwds)
        self.fieldnames = self.reader.fieldnames

    def next(self):
        row = self.reader.next()
        return dict((unicode(key, 'utf-8'), unicode(value, 'utf-8')) for key, value in row.iteritems())

    def __iter__(self):
        return self

class UnicodeDictWriter(object):
    '''
    A CSV writer which will write rows to CSV file "f", which is encoded in the given
    encoding. The default DictWriter does not support encodings.
    '''
    def __init__(self, f, fieldnames, dialect=csv.excel, encoding="utf-8", **kwds):
        # Redirect output to a queue
        self.queue = cStringIO.StringIO()
        self.writer = csv.DictWriter(self.queue, fieldnames, dialect=dialect, **kwds)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()

    def writerow(self, row):
        self.writer.writerow(dict((key, val.encode("utf-8")) for key, val in row.iteritems()))
        # Fetch UTF-8 output from the queue ...
        data = self.queue.getvalue()
        data = data.decode("utf-8")
        # ... and reencode it into the target encoding
        data = self.encoder.encode(data)
        # write to the target stream
        self.stream.write(data)
        # empty queue
        self.queue.truncate(0)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)

class LogFactory(object): # pylint: disable=too-few-public-methods
    def __init__(self, instance_name=None, log_file=None, max_backups=None, log_level=None, env='TEST', **kwargs):
        self.NAME = instance_name
        self.log_level = vars(logging)[log_level]
        self.env = env

        # Formatters
        standard_format = '%(asctime)s LogLevel=%(levelname)s Component="%(name)s" Message=%(message)s Environment=%(env)s'
        notifications_format = standard_format + ' MessageID="%(msgid)s" MessageType="%(messagetype)s" EventKey="%(eventkey)s"'

        splunk_formatter_notifications = logging.Formatter(notifications_format)
        splunk_formatter = logging.Formatter(standard_format)

        # Handlers
        self.rotating_file_notifications_handler = logging.handlers.TimedRotatingFileHandler(log_file, backupCount=max_backups, when='midnight')
        self.rotating_file_notifications_handler.setFormatter(splunk_formatter_notifications)

        self.rotating_file_handler = logging.handlers.RotatingFileHandler(log_file, maxBytes=10485760, backupCount=max_backups)
        self.rotating_file_handler.setFormatter(splunk_formatter)

    def getLogger(self, logger_name, notification=None, **kwargs):
        if logger_name == 'NotificationSender':
            logger_obj = logging.getLogger(logger_name)
            logger_obj.addHandler(self.rotating_file_notifications_handler)
            logger_obj.setLevel(self.log_level)

            return logging.LoggerAdapter(logger_obj, {
                'msgid': notification.get_msgid(),
                'messagetype': notification.get_message_type(),
                'eventkey': notification.get_event_key(),
                'env': self.env
            })

        else:
            logger_obj = logging.getLogger(logger_name)
            logger_obj.addHandler(self.rotating_file_handler)
            logger_obj.setLevel(self.log_level)

            return logging.LoggerAdapter(logger_obj, {
                'env': self.env
            })

class ConnectionHandler(object):
    '''
    Handles a connection to the ZMQ Server and all the related objects: Poller, Client, etc.
    '''
    def __init__(self, zmq_server=None, log_factory=None, instance_name=None, **kwargs):
        self.zmqserver = zmq_server
        self.identity = 'notification_spooler-%s' % (instance_name.lower())
        self.logger = log_factory.getLogger(self.__class__.__name__)

        self.refreshContext()
        self.refreshPoller()
        self.connectClient()

    def getClient(self):
        if not ('client' in vars(self).keys() and isinstance(self.client, zmq.Socket)):
            self.connectClient()

        return self.client

    def getPoller(self):
        if not ('poller' in vars(self).keys() and isinstance(self.poller, zmq.Poller)):
            self.refreshPoller()

        return self.poller

    def connectClient(self):
        # Connessione al server
        self.client = self.context.socket(zmq.REQ)
        self.client.identity = self.identity.encode('utf-8')
        self.client.connect(self.zmqserver)

        # Poller to avoid waiting forever in case the server
        # goes down before it could send us a confirmation
        self.poller.register(self.client, zmq.POLLIN)

        if not self.client.closed:
            self.logger.info('Connected')

    def disconnectClient(self):
        self.poller.unregister(self.client)
        self.client.setsockopt(zmq.LINGER, 0)
        self.client.close()

        if self.client.closed:
            self.logger.info('Disconnected')

    def refreshContext(self):
        self.context = zmq.Context()

    def refreshPoller(self):
        self.poller = zmq.Poller()

class NotificationSender(object):
    '''
    sends XML message to a remote ZMQ server and expect a reply
    '''
    def __init__(self, message=None, token=None, log_factory=None, connection=None, **kwargs):
        self.TOKEN = token
        self.xmlmsg = etree.fromstring(message)
        self.strmsg = message
        self.msgid = self.get_msgid()
        self.logger = log_factory.getLogger(self.__class__.__name__, self)

        # ZMQ Stuff
        self.connection = connection
        self.client = self.connection.getClient()

    def __get_attr(self, attr):
        '''
        Returns the UTF-8 econded text from one of the XML message data tags
        '''
        return self.xmlmsg.find(attr).text.encode('utf-8')

    def get_event_key(self):
        '''
        EventKey is the unique key for this event. It is always a sha256 hash of
        distinctive attributes + salt: which attributes are used depends on the 
        specific event. A common case is "error type" and "host", but they could
        get more specific as needed. The value is calculated by Splunk inside the
        scheduled SPL query
        '''
        return self.__get_attr('data/EventKey')

    def get_message_type(self):
        '''
        MessageType contains the name of the scheduled search. It represents
        the "type" of message being sent
        '''
        return self.__get_attr('data/MessageType')

    def get_msgid(self):
        '''
        MessageID is meant to be unique for a single message content & timestamp. It is
        used server-side to reject already processed messages that may have been sent
        multiple times for whatever reason. This feature can be disabled on the server.
        '''
        if 'msgid' in vars(self).keys() and self.msgid:
            return self.msgid
        else:
            id_cleartext = self.xmlmsg.find('data/MessageType').text + '@' + \
                       self.xmlmsg.find('data/RCH').text + "@" + \
                       self.xmlmsg.find('data/EventKey').text + "@" + \
                       self.xmlmsg.find('data/Severity').text + "@" + \
                       self.xmlmsg.find('data/Timestamp').text

            return hashlib.sha256(id_cleartext.encode('utf-8')).hexdigest()

    def send_message(self):
        '''
        Sends a 3-part 0MQ multipart message: a MessageId, message data (bytes, strings must
        be utf-8 encoded) and a checksum of the two previous parts plus a "salt", used as
        an authentication token between client and server, joined by a "00" byte.
        '''
        debug = False

        payload = u"%s\x00" % self.TOKEN + "\x00".join([self.msgid, self.strmsg])
        digest = hashlib.sha256(payload).hexdigest()
        self.logger.debug('"calculating digest" Digest="%s"', digest)

        if debug:
            self.logger.debug('Fake sending of message: ID: %s -- MESSAGE: %s -- DIGEST: %s' % (self.msgid, self.strmsg, digest))
            return True

        # Send without blocking: in case the server cannot receive the message
        # this will still asynchrnously raise a ZMQError with a errno of zmq.EAGAIN
        self.client.send_multipart([self.msgid, self.strmsg, digest])
        self.logger.debug('"Message sent"')

        # Waits for up to 5 seconds for the server to senda a confirmation message
        # or raises an exception
        sockets = dict(self.connection.poller.poll(5000))

        try:
            if self.client in sockets and sockets[self.client] == zmq.POLLIN:
                self.logger.debug('"Reading response from the server"')
                (replyid, reply) = self.client.recv_multipart()

            else:
                raise ZMQError(zmq.EAGAIN)

            # First checks if this is the correct reply for our message
            if replyid == self.msgid:
                # Then checks if the reply code is OK or KO
                if reply == 'OK':
                    self.logger.info('"Message delivered"')
                    return True
                elif 'KO' in reply:
                    error = reply.split(':')[1]
                    self.logger.error('"Message NOT delivered" Error="%s"', error)
                    return False
            else:
                self.logger.warning('"Ouf-of-sequence response from server" ReplyID="%s" Reply="%s"', replyid, reply)
                self.logger.error('"Message NOT delivered" Error="OUT_OF_SEQUENCE_REPLY"')
                return False

        except ZMQError as z:
            self.logger.error('"Message NOT delivered" Error="ZMQ_EXCEPTION" Exception="%s"' % str(z))

            if z.errno == zmq.EAGAIN:
                self.logger.info('"Trying to reconnect: disconnecting..."')
                self.connection.disconnectClient()
                time.sleep(1)
                self.logger.info('"Trying to reconnect: connecting..."')
                self.connection.connectClient()

            return False

class CsvProcessor(object):
    '''
    Takes 2 file descriptors, to be read and written usind UnicodeDictReader/Writer. The purpose
    is to be able to read a CSV file, handling the conversion of every line to an XML format and
    the eventual writing of errors or output to another CSV file
    '''
    def __init__(self, csv_file=None, error_dir=None, **kwargs):
        self.csv_file = csv_file
        self.error_dir = error_dir
        self.error_file_name = None

        # This wil register two open filehandles, csvfile and errorfile
        self.open_files()

        # CSV Input reader.
        self.reader = UnicodeDictReader(self.csvfile)

        # CSV Error writer. It needs to now in what order it should write
        # the fields, since a Dict has no way to guarantee ordering
        self.error_writer = UnicodeDictWriter(self.errorfile, self.reader.fieldnames)

    def toXml(self, search_data, rewind=False):
        '''
        Takes a list of dicts data structure (created by a CSV DictReader) and converts
        it to a list of XML strings with some additional contextual data appendend to
        each line
        '''
        output = []

        if rewind:
            self.csvfile.seek(0, 0)

        for row in self.reader:
            # The CSV file is basically converted to an easy to read XML format.
            # That is, easy for HP's BSM Connector software, not in general.
            event = etree.Element('event')
            data = etree.SubElement(event, "data")

            for key, value in row.iteritems():
                # For every field of the CSV, we create an XML element.
                # Some small sanitization is necessary to convert arbitrarily
                # named fields to XML elements with valid names.
                etree.SubElement(data, key.replace(" ", "_")).text = etree.CDATA(unicode(value))

            # Appends four fixed subelements, from the commandline
            etree.SubElement(event, "search_name").text = etree.CDATA(unicode(search_data["name"]))
            etree.SubElement(event, "search_reason").text = etree.CDATA(unicode(search_data["reason"]))
            etree.SubElement(event, "search_url").text = etree.CDATA(unicode(search_data["url"]))
            etree.SubElement(event, "search_host").text = etree.CDATA(unicode(search_data["host"]))

            # Dump the XML to a string for sending. Make it readable to help
            # debugging the Connector "policies" (parser configurations)
            output.append(etree.tostring(event, pretty_print=True, encoding='utf-8'))

        return output

    @staticmethod
    def fromXml(message):
        '''
        Converts a single XML string or Etree object "data" element contents to a dict.
        Does not handle attributes or lists of identical tags.
        '''
        if not isinstance(message, etree._Element): # pylint: disable=protected-access
            if isinstance(message, str):
                message = etree.fromstring(message)
            else:
                raise ValueError('Cannot convert message to XML')

        output = {}

        for element in message.findall("data/*"):
            output[element.tag] = element.text

        return output

    def write_errors(self, row):
        '''
        Write some data to a CSV output file. The input data must be an XML string or
        a dict and it is meant to represent messagese rejected from the 0MQ server.
        The purpose is to record errors for later easy resend when the problem is corrected
        '''

        # If the error output file has not yet been written, we have to write
        # the header line (the fields names).
        if self.errorfile.tell() == 0:
            # Python 2.7 has a "writeheader" method for DictWriter. Python 2.6 has not,
            # so we write the header line explicitly for compatibility resons.
            self.errorfile.write(",".join(self.reader.fieldnames) + "\n")

        if isinstance(row, dict):
            self.error_writer.writerow(row)
        elif isinstance(row, str):
            self.error_writer.writerow(CsvProcessor.fromXml(row))

    def close_files(self):
        self.csvfile.close()
        self.errorfile.close()

    def get_error_file_name(self):
        if 'error_file_name' in vars(self).keys() and self.error_file_name:
            return self.error_file_name
        else:
            error_file_name = '%s/%s.%d.retry.gz' % ( \
                self.error_dir,
                '.'.join(os.path.basename(self.csv_file).split('.')[:-1]), \
                int(time.time()))

            return error_file_name

    def open_files(self):
        self.error_file_name = self.get_error_file_name()

        # Open the files and get to work
        self.csvfile = gzip.open(self.csv_file, 'rb')
        self.errorfile = gzip.open(self.error_file_name, 'wb')


# MAIN Module level functions
def send_messages(**kwargs):
    '''
    cycle over the lines of a gzipped CSV file and send one message per record, recording
    messages not correctly received in a separate CSV file
    '''

    # Create the two main objects instances
    csv_processor = CsvProcessor(**kwargs)
    mainLogger = kwargs.get('log_factory').getLogger('Main')

    try:
        # Get the events as a list of XML strings
        messages = csv_processor.toXml(kwargs.get('search_data'))
        mainLogger.info('"Sending Messages" BatchSize=%d' % len(messages))
        for message in messages:
            # Create a Sender object
            sender = NotificationSender(message=message, **kwargs)

            # One row = one xml message that is sent over to the Connector/0MQ server
            # Send the message and check for its correct reception
            if not sender.send_message():
                # We could'nt contact the server OR the server said something
                # is wrong with the message. In this case, we park the original
                # CSV line to an error file for easy resending after the problem
                # has been resolved.
                csv_processor.write_errors(message)

    finally:
        # Always close the CSV files
        csv_processor.close_files()
        # Cleanup the socket
        opts['connection'].disconnectClient()

    # As is written, the code always creates an empty error output file:
    # this removes it if it has not been populated with errors.
    error_file_name = csv_processor.get_error_file_name()

    num_lines = sum(1 for line in gzip.open(error_file_name, 'r'))

    if num_lines < 2:
        os.unlink(error_file_name)

if __name__ == '__main__':

    # Main options: postitional cmd line and environment arguments parsing
    opts = {
        'instance_name': os.getenv('NOTIFICATIONS_NAME', 'SplunkNotificationSender'),
        'csv_file': sys.argv[1],
        'search_data' : {
            'name':   sys.argv[2],
            'reason': sys.argv[3],
            'url':    sys.argv[4],
            'host':   socket.gethostname(),
        },
        'env': os.getenv('NOTIFICATIONS_ENV', 'TEST'),
        'zmq_server': os.getenv('NOTIFICATIONS_SERVER', "tcp://mxtwbsmco01.mxs.svi.it:5570"),
        'token': os.getenv('NOTIFICATIONS_TOKEN', 'Q8fHuYGtOf7IgpdzTbQa')
    }

    # Working directories: make sure they exists
    opts['sent_dir'] = os.getenv('NOTIFICATIONS_SENTDIR', "run/%s/sent"  % opts['instance_name'])
    opts['error_dir'] = os.getenv('NOTIFICATIONS_ERRORDIR', "run/%s/error" % opts['instance_name'])

    if not os.path.exists(opts['sent_dir']):
        os.makedirs(opts['sent_dir'])

    if not os.path.exists(opts['error_dir']):
        os.makedirs(opts['error_dir'])

    # Create and store one instance of the logger factory in the opts
    opts['log_file'] = os.getenv('NOTIFICATIONS_LOG', "log/%s.log" % opts['instance_name'])
    opts['max_backups'] = os.getenv('NOTIFICATIONS_MAX_BACKUP', 5)
    opts['log_level'] = os.getenv('NOTIFICATIONS_LOG_LEVEL', 'INFO')
    opts['log_factory'] = LogFactory(**opts)

    # Create and store one instance of the connection handler in the opts.
    # Order is important! ConnectionHandler expects an instance of LogFactory in the opts.
    opts['connection'] = ConnectionHandler(**opts)

    # Send those messages!
    send_messages(**opts)
