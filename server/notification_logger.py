#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import os.path
import sys
import time
import signal
import hashlib
import logging
import logging.handlers
import zmq
from zmq import ZMQError
import redis

class LRUCache(object):
    '''Objects cache with a timestamp, has a LRU cleanup policy'''
    def __init__(self, db):
        self.redis = redis.StrictRedis(host='localhost', port=6379, db=db)

    def get(self, key):
        try:
            value = self.redis.get(key)
            return value
        except redis.exceptions.ConnectionError:
            return None

    def set(self, key, value):
        try:
            self.redis.set(key, value)
        except redis.exceptions.ConnectionError:
            pass

    def dump(self):
        try:
            for key in self.redis.scan_iter("*"):
                print "%s: %s" % (key, self.redis.get(key))
        except redis.exceptions.ConnectionError:
            pass

    def size(self):
        try:
            return self.redis.dbsize()
        except redis.exceptions.ConnectionError:
            return 0

class ServerTask(object):
    '''Very simple ZMQ REQ/REP Server with some authentication/integrity control'''

    def __init__(self):

        self.kill_now = False

        self.name = os.getenv('LOGGER_NAME')
        self.logdir = os.getenv('LOGGER_LOGDIR','logs')
        self.output = os.getenv('LOGGER_OUTPUT')
        self.token = os.getenv('LOGGER_TOKEN')
        self.port = int(os.getenv('LOGGER_PORT', 5570))
        self.address = os.getenv('LOGGER_ADDRESS', '*')
        self.max_size = os.getenv('LOGGER_MAX_SIZE', 10*1024*1024)
        self.backups = os.getenv('LOGGER_BACKUPS', 5)

        cache_db = os.getenv('LOGGER_LRU_CACHE_ID')

        if '-s' in sys.argv:
            self.STRICT_MODE = True
        else:
            self.STRICT_MODE = False

        if '-v' in sys.argv:
            self.VERBOSE_MODE = True
        else:
            self.VERBOSE_MODE = False

        self.logger = self.setup_logging()

        self.logger.info("Starting")

        self.output_writer = self.setup_output_writer()

        self.logger.debug("Configuration dump")
        for key, value in vars(self).iteritems():
            if type(value) == str:
                self.logger.debug("%s: %s" % (str(key), value))

        self.LRUCache = LRUCache(cache_db)
        self.logger.debug('LRUCache initialized with %d elements' % self.LRUCache.size())

        self.context = zmq.Context()
        self.logger.debug('Got a context from ZMQ')

    def setup_logging(self):
        '''Returns a configured logger'''
        logger = logging.getLogger(self.name)

        # Logging levels setup
        if self.VERBOSE_MODE:
            level = logging.DEBUG
        else:
            level = logging.INFO
        logger.setLevel(level)

        # Rotate log at midnight, keep 5 backups
        logfile = '%s\\notifications_%s.log' % (self.logdir, self.name)
        loghandler = logging.handlers.TimedRotatingFileHandler(logfile, when='midnight', backupCount=5)
        loghandler.setFormatter(
            logging.Formatter(
                '%(asctime)s Name=%(name)s Level="%(levelname)s" Pid="%(process)d" Message="%(message)s"'))
        logger.addHandler(loghandler)

        # On Windows, log on the EventLog too
        if sys.platform.startswith('win'):
            # Events will appear as originating from $LOGGER_NAME
            nthandler = logging.handlers.NTEventLogHandler(self.name, logtype='Application')
            logger.addHandler(nthandler)

        return logger

    def setup_output_writer(self):
        '''Returns a writer that appends to a file, handling roll-over when it reaches max_size'''
        output_writer = logging.getLogger("%s-output-writer" % self.name)
        output_writer.setLevel(logging.INFO)
        output_file = "%s\\%s" % (self.logdir, self.output)
        output_handler = logging.handlers.RotatingFileHandler(
            output_file, mode='a', maxBytes=self.max_size, encoding='utf-8', backupCount=self.backups)
        output_handler.setFormatter(logging.Formatter('%(message)s'))
        output_writer.addHandler(output_handler)

        return output_writer

    def install_signal_handlers(self):
        '''Install signal handlers to asynchronously intercept exit requests'''
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)
        signal.signal(signal.SIGHUP, self.exit_gracefully)
        signal.signal(signal.CTRL_BREAK_EVENT, self.exit_gracefully)
        signal.signal(signal.CTRL_C_EVENT, self.exit_gracefully)

    def exit_gracefully(self, signo, frame):
        '''Logs the request and change the internal exit flag'''
        self.logger.debug('Exit requested')
        self.kill_now = True

    def run(self):
        '''Main server loop'''
        self.logger.info('Notifications Logger starting for: %s', self.name)

        # Setup a listening socket
        socket = self.context.socket(zmq.REP)
        socket.bind('tcp://%s:%d' % (self.address, self.port))
        self.logger.debug("Succesfully bound ZMQ REP server on %s:%d" % (self.address, self.port))

        poller = zmq.Poller()
        poller.register(socket, zmq.POLLIN)

        while True:
            # Handle exit requests
            if self.kill_now:
                self.logger.info('Exiting now')
                break

            try:
                # Wait half a second for a message to come
                if not poller.poll(500):
                    # If there are no messages, continue the loop
                    continue

                # If there are messages...
                message = socket.recv_multipart()

                self.logger.info("Received message: %s - %d parts" % (message[0], len(message)))
                self.logger.debug("Dumping raw message: %s " % " : ".join(message))

                # Splits the notification content from it's ID. Themessage should
                # always contain 3 parts, but the checksum can be void string
                msgid, content, received_checksum = message

                # If the client sent along a checksum, verify it now
                if received_checksum:
                    self.logger.debug("Calculating checksum")
                    # Form the payload string
                    payload = "%s\x00" % self.token + "\x00".join(message[0:2])
                    calculated_checksum = hashlib.sha256(payload).hexdigest()
                    self.logger.debug("Received checksum: %s"  % received_checksum)
                    self.logger.debug("Calculated checksum: %s"  % calculated_checksum)
                    if not calculated_checksum == received_checksum:
                        raise ValueError('corrupted_message')

                # Also raises ValueError if we only received a partial message
                if not (msgid and content):
                    raise ValueError('invalid_format')

                # In strict mode we refuse duplicate messages
                if self.STRICT_MODE:
                    received_at = self.LRUCache.get(msgid)
                    if received_at:
                        self.logger.error('Message %s was already received at: %s', msgid, received_at)
                        raise ValueError('duplicated_message')

                    # Sets or update this message's entry in the cache
                    self.LRUCache.set(msgid, time.asctime())

                self.logger.debug('Message %s succesfully validated' % msgid)

                # Write the message to disk, possibly rotating the file
                self.output_writer.info(unicode(content,'utf-8'))

                self.logger.debug('Message %s succesfully written' % msgid)

                # Sends confirmation of reception to the client
                socket.send_multipart([msgid, 'OK'])

                self.logger.info('Sent confirmation for message %s ' % msgid)

            ### Recoverable situations ###

            except ZMQError as z:
                self.logger.error(
                    'Cannot send acknowledgement for message: %s (%s)', msgid, str(z))

                continue

            except ValueError as v:
                if 'to unpack' in v.message:
                    msgid = msgid or None
                    self.logger.error('Not enough or too many parts in multipart message')
                    socket.send_multipart([msgid, 'KO:INVALID_FORMAT'])

                elif str(v) == 'duplicated_message':
                    self.logger.error('Duplicate message: %s', msgid)
                    socket.send_multipart([msgid, 'KO:DUPLICATED_MESSAGE'])

                elif str(v) == 'corrupted_message':
                    self.logger.error('Message is unauthenticated, corrupted or has been tampered with')
                    socket.send_multipart([msgid, 'KO:CORRUPTED_MESSAGE'])

                else:
                    self.logger.error('Generic error in message:\n%s', message)
                    socket.send_multipart([msgid, 'KO:GENERIC_ERROR'])

                continue

            ### Unrecoverable situations ###

            # TODO: this assumes the filesystem is the only thing that can throw IOErrors
            except IOError:
                exc_value = sys.exc_info()[1]
                self.logger.error('Exception while trying to append message to output %s: %s', msgid, exc_value)
                socket.send_multipart([msgid, 'KO:CANNOT_WRITE_OUTPUT'])
                break

            #  Interrupt from console, when running in the foreground
            except KeyboardInterrupt:
                self.logger.info('Exit requested via KeyboardInterrupt, exiting now')
                break

        socket.setsockopt(zmq.LINGER, 0)
        socket.close()

if __name__ == '__main__':
    server = ServerTask()
    server.run()
