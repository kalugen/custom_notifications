Installazione
=============

Windows
-------

1. Installare il software
    * Installare MSI Redis (6379) set 100mb
    * Installare MSI Python 2.7 (PATH)
    * Installare MSI pyzmq (librerie ZMQ + client python, ma obsoleto)

2. Configurare Redis:
    * File C:\Program Files\Redis\redis.windows-service.conf
    * Aggiungere "maxmemory-policy allkeys-lru" per attivare gestione LRU della memoria
    * Riavviare servizio Redis

Aprire riga di comando su directory "Installation"

3. Installare librerie python: da riga di comando "pip install <filename>.whl"
    * pywin32 (estensioni win32 per python)
    * pyzmq (aggiornamento librerie rispetto a quelle installate prima)
    * redis (client redis per python)

Le prossime operazioni sono **DA RIPETERE PER OGNI SERVIZIO CHE SI DESIDERA CREARE**

NB: un file di output = una porta TCP = un servizio

Da riga di comando, sempre nella directory "Installation", Installare e configurare il servizio: 

`nssm install <service_name>`

Configurarlo come segue:
* Assegnare come eseguibile `C:\Python27\python.exe`
* Startup dir: `C:\notification_logger`
* Arguments: `C:\notification_logger\notification_logger.py -s -v`
* Compilare l'environment come da esempio:

```bash
LOGGER_PORT=5570
LOGGER_NAME=mis_notifications_logger_splunk
LOGGER_LOGDIR=logs
LOGGER_TOKEN=GxIrkOA5hHea19DD3kwT
LOGGER_OUTPUT=splunk_notifications.xml
LOGGER_LRU_CACHE_ID=0
LOGGER_MAX_SIZE=10485760
LOGGER_BACKUPS=3
```
 
Dove:
   - PORT è la porta TCP dove ascolta il servizio
   - NAME è il nome del servizio (compare nei log)
   - LOGDIR è la directory di log, relativa a Startup Dir
   - OUTPUT è il nome del file di output, sempre relativo a Startup Dir
   - TOKEN è il token che il client deve passare, rudimentale autenticazione
   - BACKUPS è il numero di file di output di backup che vanno tenuti (ruotano ogni 10 Mb)
   - LRU_CACHE_ID è il numero da 0 a 15 del DB Redis da usare come cache LRU
 
Linux
-----

to be written

