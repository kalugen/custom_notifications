# Simple messaging system

Simple client/server based on 0MQ sockets, with a REQ-REPL topology with sample implementations.

The server is generic and expects a 3-parts message, made of:
- message ID
- message body
- checksum

The message ID is set by the client, it is meant for deduplication: the server uses a Redis database as a LRU cache to ensure that the same message ID is not processed twice (of a resonable amount of time, depending by the size of the Redis instance). How the client calculates this ID is enterely up to whatever needs to be used for correct deduplication of the specific messages being transported: time of day, some data or metadata in the message, some external information or something else entirely.

The message body can be any UTF-8 text, of any lenght. The current implementation of the server will just append it to a size-rotated log.

The checksum is a sha256 hash, calculated on the message body, message ID plus a secret token known to the server and client and never transmitted. This creates a simple layer of authentication/integrity checking, with no privacy at all (messages themselves are not encrypted). Further work is needed to switch to the native cryptographic layer of 0MQ.

## Splunk to BMSC (XML log file policy)

This is a client meant to work on Splunk's gzipped CSV files, as created by scheduled searches. It expects a gzipped CSV as input and translates every row to an XML format, devised to be easily read by BSMC "XML policy". This way, Splunk searches can easily send "events" to BSM for further processing (for us, the opening of an incident ticket on ServiceNow via Applink's CLIP).

Failed messages due to duplicates, corrupted data or unavailable server errors are saved in compressed CSV files, so that after correcting the root problem they can be easily resent.

There is an Ansible playbook designed to deploy all the needed tools and scripts to our Splunk search head servers, in a way that wuold create different "alert scripts" to send events to different BSMC deployments (eg. TEST and PROD).

## CheckMK notifications to BSMC (XML log file policy)

This client is quite similar to the Splunk's one, except for the specifics of the messages being transmitted and the way they are collected: this client will look for files under a spooling directory and on finding them it will send them to the server as-is.

The client is meant to be left running as a daemon, with a single daemon for every CheckMK site on the host machine. The files that would populate the spooling directory are created by Check_MK by way of a notification script that produces XML files ready to be read by the BSMC policy, so they are in effect notification messages and thus are subject to CheckMK's notification logic, not the simpler, less flexbile internal events one. 

The main purpose here was to superceed HP's own proprietary integration with Nagios using a NEB module, substituting it with a more flexible and controllable mechanism.


